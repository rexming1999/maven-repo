import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.Test;

import io.tatum.ledger.LedgerAccount;
import io.tatum.model.request.CreateAccount;
import io.tatum.model.request.Currency;
import io.tatum.model.wallet.Wallet;

public class TestLedgerAccount {

	@Test
	public void createWallet() throws IOException, ExecutionException, InterruptedException {
		Wallet wallet = new Wallet();
		wallet.setAddress("0xd2626e41c6ff47f05e12fa839b7dccdb5a8be386");
		wallet.setMnemonic("yellow flame believe tortoise invite tell put fade slight select mercy visual");
		wallet.setSecret("0x0a91d60fe2d2cb0998d90cae772373c837d1521cdd85bc58ef6e0bafe2763dc8");
		wallet.setXpub("xpub6EJQHzDYqBMS93iE8cfaLgTX25Cww1LnhxNqBoibbCGZJvmdhkeg5C8cNGs5KmTmwtELCiPGxd6gt8tobU9iaxy1RibD1WkJNMHBeGR9FyL");
		
		CreateAccount createAccount = new CreateAccount();
		createAccount.setCurrency(Currency.ETH.getCurrency());
		createAccount.setXpub(wallet.getXpub());
		createAccount.setCompliant(false);
		
		io.tatum.model.response.ledger.Account account = new LedgerAccount().createAccount(createAccount);
		
		System.out.println(account.getId());
	}
}
	